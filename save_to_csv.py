import csv

from typing import List

from saver_message import ParsedMessage


def save_to_csv(messages: List[ParsedMessage], filename: str):
    fields = list(ParsedMessage().__dict__.keys())

    filename = f"{filename}.csv"

    with open(filename, 'w') as csvfile:
        # creating a csv dict writer object
        writer = csv.DictWriter(csvfile, fieldnames=fields)
        writer.writeheader()
        for key in messages:
            writer.writerow(key.__dict__)
