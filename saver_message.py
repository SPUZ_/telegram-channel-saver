import asyncio
from dataclasses import dataclass
from datetime import datetime
from typing import Optional, Union, List

from pyrogram import Client
from pyrogram.enums import MessageMediaType
from pyrogram.types import Message


@dataclass
class ParsedMessage:
    id: int = 0
    date: Optional[datetime] = None
    is_empty: bool = False
    text: str = ""
    views: int = 0
    date_media: Optional[str] = None
    is_media: Union[bool, str] = False
    file_id: Optional[str] = None
    media_name: Optional[str] = None
    download_name: Optional[str] = None
    longitude: Optional[int] = None
    latitude: Optional[int] = None
    phone_number_contact: Optional[str] = None
    first_name_contact: Optional[str] = None
    last_name_contact: Optional[str] = None
    user_id_contact: Optional[int] = None


def saver_message(message: Message, app: Client, chat_id: int):
    parsed = ParsedMessage(message.id, message.date, message.empty,
                           message.text, message.views, None,
                           False, None, None, None,
                           None, None, None,
                           None, None, None)

    # TODO: forwared
    # TODO: removed
    # TODO: edited

    if message.media is not None:
        parsed.text = message.caption

        if message.media is MessageMediaType.PHOTO:
            parsed.is_media = "photo"
            parsed.file_id = message.photo.file_id
            parsed.download_name = message.photo.file_unique_id + ".jpg"
            parsed.date_media = message.photo.date
        if message.media is MessageMediaType.ANIMATION:
            parsed.is_media = "gif"
            parsed.file_id = message.animation.file_id
            parsed.download_name = message.animation.file_unique_id
            parsed.date_media = message.animation.date
        if message.media is MessageMediaType.VOICE:
            parsed.is_media = "voice"
            parsed.file_id = message.voice.file_id
            parsed.download_name = message.voice.file_unique_id
            parsed.date_media = message.voice.date
        if message.media is MessageMediaType.VIDEO_NOTE:
            parsed.is_media = "video circle"
            parsed.file_id = message.video_note.file_id
            parsed.download_name = message.video_note.file_unique_id
            parsed.date_media = message.video_note.date
        if message.media is MessageMediaType.AUDIO:
            parsed.is_media = "audio"
            parsed.file_id = message.audio.file_id
            parsed.download_name = message.audio.file_unique_id
            parsed.media_name = message.audio.file_name
            parsed.date_media = message.audio.date
        if message.media is MessageMediaType.VIDEO:
            parsed.is_media = "video"
            parsed.file_id = message.video.file_id
            parsed.download_name = message.video.file_unique_id
            parsed.media_name = message.video.file_name
            parsed.date_media = message.video.date
        if message.media is MessageMediaType.DOCUMENT:
            parsed.is_media = "doc"
            parsed.file_id = message.document.file_id
            parsed.download_name = message.document.file_unique_id
            parsed.media_name = message.document.file_name
            parsed.date_media = message.document.date
        if message.media is MessageMediaType.LOCATION:
            parsed.is_media = "location"
            parsed.longitude = message.location.longitude
            parsed.latitude = message.location.latitude
        if message.media is MessageMediaType.CONTACT:
            parsed.is_media = "contact"
            parsed.phone_number_contact = message.contact.phone_number
            parsed.first_name_contact = message.contact.first_name
            parsed.last_name_contact = message.contact.last_name
            parsed.user_id_contact = message.contact.user_id

    return parsed


def download_media_fast(media_name: List[List[str]], app: Client, chat_id: int):
    loop = asyncio.get_event_loop()
    tasks = [loop.create_task(download_media(md[0], md[1], app, chat_id)) for md in media_name]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()


async def download_media(media_name: str, download_file: str, app: Client, chat_id: int):
    await app.download_media(media_name, f"storage/{chat_id}/{download_file}")
