import os
from typing import List

from pyrogram import Client, filters
from pyrogram.enums import MessageMediaType
from pyrogram.types import Message

from datetime import datetime

import save_to_csv
import saver_message

app = Client("account")

app.start()

chat_id = os.getenv("CHAT_ID")
try:
    chat_id = int(chat_id)
except ValueError as err:
    print(err)
    print("chat_id must be int")
    exit()

try:
    channel = app.get_chat(chat_id)
except Exception as ex:
    print("I can't find the channel, subscribe to this channel first")
    exit()


date_now = datetime.now()


def messages_are_empty(msgs: List[Message]) -> bool:
    bad_messages = 0
    for m in msgs:
        if m.empty:
            bad_messages += 1
    if bad_messages == len(msgs):
        return True
    else:
        return False


message_id = [0]
to_save = []
is_empty_count = 0
while True:
    message_id = list(range(message_id[-1] + 1, message_id[-1] + 201))
    print(message_id)

    mess: List[Message] = app.get_messages(chat_id, message_id)
    is_empty = messages_are_empty(mess)

    if is_empty:
        is_empty_count += 1
    else:
        is_empty_count = 0

    if is_empty_count > 3:
        break

    for message in mess:
        to_save.append(saver_message.saver_message(message, app, chat_id))


save_to_csv.save_to_csv(to_save, f"storage/{chat_id}")
